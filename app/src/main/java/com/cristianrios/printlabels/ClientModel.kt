package com.cristianrios.printlabels

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ClientModel(
    var id: Int,
    var name: String,
    var surname: String,
    var address: String,
    var zipCode: Int,
    var phone: String,
    var dni: String,
    var isSelected: Boolean = false) : Parcelable {


    override fun toString(): String{
        return "{id = $id, full name = $name $surname, is selected = $isSelected}"
    }
}