package com.cristianrios.printlabels


import android.content.ContentValues
import android.content.DialogInterface
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.ListView
import androidx.appcompat.app.AlertDialog
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    private var listClients = ArrayList<ClientModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btCreate.setOnClickListener { view ->
            var intent = Intent(this, ClientActivity::class.java)
            startActivity(intent)
        }

        loadQueryAll()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)

        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item != null) {
            when (item.itemId) {
                R.id.miPrint -> {
                    var clientsSelected = listClients.filter { client -> client.isSelected }
                    if (clientsSelected.isEmpty()) {
                        val alertDialog: AlertDialog? = this.let {
                            val builder = AlertDialog.Builder(it)
                            builder.apply {
                                setPositiveButton(R.string.ok, null)
                            }
                            // Set other dialog properties
                            builder.setTitle(R.string.print_title)
                            builder.setMessage(R.string.print_message_empty)
                            // Create the AlertDialog
                            builder.create()
                        }
                        alertDialog?.show()
                    }else {
                        val alertDialog: AlertDialog? = this.let {
                            val builder = AlertDialog.Builder(it)
                            builder.apply {
                                setPositiveButton(R.string.ok, DialogInterface.OnClickListener { _, _ ->
                                    doClientsPrint(it, clientsSelected)
                                })
                                setNegativeButton(R.string.cancel, null)
                            }
                            // Set other dialog properties
                            builder.setTitle(R.string.print_title)
                            builder.setMessage(resources.getString(R.string.print_message_clients, clientsSelected.size))
                            // Create the AlertDialog
                            builder.create()
                        }
                        alertDialog?.show()
                    }
                }
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onResume() {
        super.onResume()
        loadQueryAll()
    }

    fun loadQueryAll() {
        var dbManager = ClientsDbManager(this)
        val cursor = dbManager.queryAll()

        listClients.clear()
        if (cursor.moveToFirst()) {

            do {
                val id = cursor.getInt(cursor.getColumnIndex("Id"))
                val name = cursor.getString(cursor.getColumnIndex("Name"))
                val surname = cursor.getString(cursor.getColumnIndex("Surname"))
                val address = cursor.getString(cursor.getColumnIndex("Address"))
                val zipCode = cursor.getInt(cursor.getColumnIndex("ZipCode"))
                val phone = cursor.getString(cursor.getColumnIndex("Phone"))
                val dni = cursor.getString(cursor.getColumnIndex("DNI"))

                listClients.add(ClientModel(id, name, surname, address, zipCode, phone, dni))

            } while (cursor.moveToNext())
        }

        var clientsAdapter = ClientsAdapter(this, listClients)
        var lvClients = findViewById<ListView>(R.id.lvClients)
        lvClients.adapter = clientsAdapter
    }
}
