package com.cristianrios.printlabels

import android.content.ContentValues
import android.os.Bundle
import android.text.TextUtils
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_client.*
import org.w3c.dom.Text


class ClientActivity : AppCompatActivity() {

    var client: ClientModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_client)

        client = intent.getParcelableExtra("client")
        if (client != null) {
            etName.setText(client!!.name)
            etSurname.setText(client!!.surname)
            etAddress.setText(client!!.address)
            etZipCode.setText(client!!.zipCode.toString())
            etPhone.setText(client!!.phone)
            etDNI.setText(client!!.dni)

            btAdd.setText(R.string.edit_btn)
        }

        btAdd.setOnClickListener {
            var values = ContentValues()
            values.put(colName, etName.text.toString())
            values.put(colSurname, etSurname.text.toString())
            values.put(colAddress, etAddress.text.toString())
            values.put(colZipCode, etZipCode.text.toString())
            values.put(colPhone, etPhone.text.toString())
            values.put(colDni, etDNI.text.toString())

            if (!isEmptyValues(values)) {
                var dbManager = ClientsDbManager(this)

                if (client == null) {
                    val mID = dbManager.insert(values)

                    if (mID > 0) {
                        showToast(R.string.add_success_message)
                        finish()
                    } else {
                        showToast(R.string.add_fail_message)
                    }
                } else {
                    var selectionArs = arrayOf(client!!.id.toString())
                    val mID = dbManager.update(values, "Id=?", selectionArs)

                    if (mID > 0) {
                        showToast(R.string.edit_success_message)
                        finish()
                    } else {
                        showToast(R.string.edit_fail_message)
                    }
                }
            } else {
                showToast(R.string.edit_required)
            }
        }
    }

    private fun showToast(message: Int) {
        Toast.makeText(this, resources.getString(message), Toast.LENGTH_LONG).show()
    }

    private fun isEmptyValues(values: ContentValues): Boolean {
        for (value in values.valueSet()) {
            if (TextUtils.isEmpty(value.value.toString())) {
                return true
            }
        }
        return false
    }
}