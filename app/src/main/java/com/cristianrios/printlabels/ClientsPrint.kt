package com.cristianrios.printlabels

import android.content.ContentValues
import android.content.Context
import android.print.PrintAttributes
import android.print.PrintManager
// import android.util.Log
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import java.lang.StringBuilder


fun doClientsPrint(context: Context, listClients: List<ClientModel>) {
    doWebViewPrint(context, clients2HTML(listClients))
}

private fun clients2HTML(listClients: List<ClientModel>): String {
    var clientsHTMLBuilder = StringBuilder()

    clientsHTMLBuilder.append("""
        <!DOCTYPE html>
        <html lang="en">
        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content=width=device-width, initial-scale=1.0">
            <style type="text/css">
                .box {
                    border: solid 3px;
                    text-align: center;
                    font-size: 15px;
                    width: 8cm;
                    padding: 10px;
                    margin: 5px;
                    display: inline-block;
                }
            </style>
            <title>Clients</title>
        </head>
        <body>
    """)
    for (client in listClients) {
        if (client.isSelected) {
            var clientHTMLBuilder = StringBuilder()
            clientHTMLBuilder.append("""
                <div class="box">
                    <p><b>Nombre:</b> ${client.name} ${client.surname}</p>
                    <p><b>DNI:</b> ${client.dni}</p>
                    <p><b>Dirección:</b> ${client.address}</p>
                    <p><b>Código postal:</b> ${client.zipCode}</p>
                    <p><b>Teléfono:</b> ${client.phone}</p>
                </div>
            """)
            clientsHTMLBuilder.append(clientHTMLBuilder)
        }
    }
    clientsHTMLBuilder.append("""
        </body>
        </html>
    """)

    return clientsHTMLBuilder.toString()
}

private fun doWebViewPrint(context: Context, content: String) {
    // Create a WebView object specifically for printing
    val webView = WebView(context)
    webView.webViewClient = object : WebViewClient() {

        override fun shouldOverrideUrlLoading(view: WebView, request: WebResourceRequest) = false

        override fun onPageFinished(view: WebView, url: String) {
            // Log.i(ContentValues.TAG, "page finished loading $url")
            createWebPrintJob(view, context)
            // mWebView = null
        }
    }

    // Generate an HTML document on the fly:
    webView.loadDataWithBaseURL(null, content, "text/HTML", "UTF-8", null)

    // Keep a reference to WebView object until you pass the PrintDocumentAdapter
    // to the PrintManager
    // mWebView = webView
}

private fun createWebPrintJob(webView: WebView, context: Context) {

    // Get a PrintManager instance
    (context.getSystemService(Context.PRINT_SERVICE) as? PrintManager)?.let { printManager ->

        val jobName = "Print Labels Document"

        // Get a print adapter instance
        val printAdapter = webView.createPrintDocumentAdapter(jobName)

        // Create a print job with name and adapter instance
        printManager.print(
                jobName,
                printAdapter,
                PrintAttributes.Builder().build()
        )
    }
}