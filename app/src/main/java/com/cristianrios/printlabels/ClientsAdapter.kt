package com.cristianrios.printlabels

import android.content.Context
import android.content.DialogInterface
import android.content.Intent
// import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.CheckBox
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AlertDialog


class ClientsAdapter : BaseAdapter {

    private var clientsList = ArrayList<ClientModel>()
    private var context: Context

    constructor(context: Context, clientsList: ArrayList<ClientModel>) : super() {
        this.clientsList = clientsList
        this.context = context
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View? {

        val view: View?
        val vh: ViewHolder

        if (convertView == null) {
            view = LayoutInflater.from(context).inflate(R.layout.client, parent, false)
            vh = ViewHolder(view)
            view.tag = vh
            // Log.i("JSA", "set Tag for ViewHolder, position: " + position)
        } else {
            view = convertView
            vh = view.tag as ViewHolder
        }

        vh.cbSelect.setOnCheckedChangeListener(null) // avoid issue row clean when scroll

        var mClient = this.clientsList[position]

        vh.tvName.text = mClient.name
        vh.tvSurname.text = mClient.surname
        vh.cbSelect.isChecked = mClient.isSelected;

        vh.ivEdit.setOnClickListener {
            updateClient(mClient)
        }

        vh.ivDelete.setOnClickListener {
            val alertDialog: AlertDialog? = this.context.let {
                val builder = AlertDialog.Builder(it)
                builder.apply {
                    setPositiveButton(R.string.ok, DialogInterface.OnClickListener {_, _ ->
                        deleteClient(mClient)
                    })
                    setNegativeButton(R.string.cancel, null)
                }
                // Set other dialog properties
                builder.setTitle(R.string.delete_title)
                builder.setMessage(it.resources.getString(R.string.delete_message, mClient.name, mClient.surname))
                // Create the AlertDialog
                builder.create()
            }
            alertDialog?.show()
        }

        vh.cbSelect.setOnCheckedChangeListener { _, isChecked ->
            mClient.isSelected = isChecked
        }

        return view
    }

    private fun deleteClient(client: ClientModel) {
        var dbManager = ClientsDbManager(context!!)
        val selectionArgs = arrayOf(client.id.toString())
        dbManager.delete("Id=?", selectionArgs)
        var activity = context as MainActivity
        activity.loadQueryAll()
    }

    private fun updateClient(client: ClientModel) {
        var intent = Intent(context, ClientActivity::class.java)
        intent.putExtra("client", client)
        context.startActivity(intent)
    }

    override fun getItem(position: Int): Any {
        return clientsList[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return clientsList.size
    }
}

private class ViewHolder(view: View?) {
    val tvName: TextView = view?.findViewById(R.id.tvName) as TextView
    val tvSurname: TextView = view?.findViewById(R.id.tvSurname) as TextView
    val ivEdit: ImageView = view?.findViewById(R.id.ivEdit) as ImageView
    val ivDelete: ImageView = view?.findViewById(R.id.ivDelete) as ImageView
    val cbSelect: CheckBox = view?.findViewById(R.id.cbSelect) as CheckBox
}