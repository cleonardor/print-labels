2021-05-29  Cristian Ríos  <cristianrios7@outlook.com>

	* Fix bug that cleared the checkbox of the selected clients when they were hidden by scrolling down or up.

	* Change color theme.

	* Improve print template generation.


2021-05-01  Cristian Ríos  <cristianrios7@outlook.com>

	* First release.